package learn.apptivitylab.com.location;

import android.*;
import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by Ralf on 10/19/2016.
 */

public class Location_Frame extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private TextView mTextViewLongtitude;
    private TextView mTextViewLatitude;
    private TextView mTextViewAccurary;
    private Button mButtonStart;
    private Button mButtonStop;

    private GoogleApiClient mGoogleApiClient;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootview = inflater.inflate(R.layout.mainframelayout,container,false);

        mTextViewLongtitude = (TextView)rootview.findViewById(R.id.mainframe_layout_tv_longtitude);
        mTextViewLatitude= (TextView)rootview.findViewById(R.id.mainframe_layout_tv_latitude);;
        mTextViewAccurary= (TextView)rootview.findViewById(R.id.mainframe_layout_tv_accuracy);;
        mButtonStart = (Button)rootview.findViewById(R.id.mainframe_layout_button_start);
        mButtonStop= (Button)rootview.findViewById(R.id.mainframe_layout_button_stop);

        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(mGoogleApiClient == null){
            mGoogleApiClient = new GoogleApiClient.Builder(getContext(),this,this)
                                .addApi(LocationServices.API)
                                 .build();
        }

        mButtonStop.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,Location_Frame.this);
            }
        });

        mButtonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLocationUpdate();
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startLocationUpdate();
        }
    }

    private void startLocationUpdate(){
        if(mGoogleApiClient.isConnected()){
            if(ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                ActivityCompat.requestPermissions(getActivity(),new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},100);
                return;
            }

            LocationRequest request = new LocationRequest();
            request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            request.setInterval(5000);//in milliseconds (5 seconds)

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,request,this);

        }else{
            Snackbar.make(getView(),"GoogleApiClient is not ready yet", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Snackbar.make(getView(),"Location update received", Snackbar.LENGTH_SHORT).show();

        String latitudeString = "Latitude:" + String.valueOf(location.getLatitude());
        String longtitudeString = "Longtitude:" + String.valueOf(location.getLongitude());
        String accuracyString = "Accuracy" + String.valueOf(location.getAccuracy()) + "m";

        mTextViewLatitude.setText(latitudeString);
        mTextViewLatitude.setText(longtitudeString);
        mTextViewAccurary.setText(accuracyString);
    }
}
